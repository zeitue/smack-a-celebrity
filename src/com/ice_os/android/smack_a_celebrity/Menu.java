package com.ice_os.android.smack_a_celebrity;


import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends Activity {
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		
		Button Start = (Button)findViewById(R.id.play);
		Start.setOnClickListener(new OnClickListener() {
		
			public void onClick(View v) {
				Intent StartIntent = new Intent(Menu.this,Smack_A_CelebrityActivity.class);
				startActivity(StartIntent);
				//finish();
			}
		});
		
		Button Help = (Button)findViewById(R.id.help);
		Help.setOnClickListener(new OnClickListener() {
		
			public void onClick(View v) {
				Intent HelpIntent = new Intent(Menu.this,Help.class);
				startActivity(HelpIntent);
			}
		});
	
		
		
		Button Options = (Button)findViewById(R.id.preferences);
		Options.setOnClickListener(new OnClickListener() {
		
			public void onClick(View v) {
				Intent OptionsIntent = new Intent(Menu.this,Preferences.class);
				startActivity(OptionsIntent);
			}
		});
	
	}
	
	public Activity getActivity(){
		return this.getActivity();
	}

}
