package com.ice_os.android.smack_a_celebrity;

import java.lang.reflect.Field;
import java.util.Random;
import com.ice_os.android.smack_a_celebrity.R.drawable;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Smack_A_CelebrityActivity extends Activity {
	/** Called when the activity is first created. */
	private int CurrentScore;
	private int CurrentLevel = 1;
	private int CurrentLives = 5;
	private static int currentCelebrityLocation = -1;
	private Handler step;
	private Handler Update;
	private GameCore GC;
	private static boolean IsCelebrity = true;
	private static GraphicsCelebrity GCelebrity;
	static int[] CelebritiesArray = new int[8];
	static int current;
	static int numberof = 0;
	static double probability = 0.95;
	static int favCelebrity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		setUp();
		final TextView ScoreTextView = (TextView) findViewById(R.id.score);
		final TextView LevelTextView = (TextView) findViewById(R.id.level);
		final TextView LivesTextView = (TextView) findViewById(R.id.life);


		final GridView SpotLightGrid = (GridView) findViewById(R.id.grid);
		step = new ChangeImage();
		Update = new Handler();
		GCelebrity = new GraphicsCelebrity(this);
		SpotLightGrid.setAdapter(GCelebrity);

		SpotLightGrid.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, final View v,
					int position, long id) {
				if (currentCelebrityLocation == position) {
					Update.post(new Runnable() {

						public void run() {
							if (IsCelebrity == true) {
								CurrentScore++;
								ScoreTextView.setText("Score: " + CurrentScore);
								ScoreTextView.refreshDrawableState();
								
								if (CurrentScore == 10) {
									GC.SetWaitTime(1200);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.90);
								} else if (CurrentScore == 20) {
									GC.SetWaitTime(1000);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.85);
								} else if (CurrentScore == 30) {
									GC.SetWaitTime(900);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.80);
								}
								else if (CurrentScore == 40) {
									GC.SetWaitTime(800);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.70);
								}
								else if (CurrentScore == 50) {
									GC.SetWaitTime(750);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.65);
								}
								else if (CurrentScore == 60) {
									GC.SetWaitTime(700);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.60);
								}
								else if (CurrentScore == 70) {
									GC.SetWaitTime(600);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.55);
								}
								else if (CurrentScore == 80) {
									GC.SetWaitTime(500);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.50);
								}
								else if (CurrentScore == 100) {
									GC.SetWaitTime(400);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.45);
								}
								else if (CurrentScore == 110) {
									GC.SetWaitTime(350);
									CurrentLevel++;
									LevelTextView.setText("Level: " + CurrentLevel);
									LevelTextView.refreshDrawableState();
									CurrentLives++;
									LivesTextView.setText("Lives: " + CurrentLives);
									LivesTextView.refreshDrawableState();
									setProbability(0.40);
								}
							}
							else
							{
								CurrentLives--;
								LivesTextView.setText("Lives: " + CurrentLives);
								LivesTextView.refreshDrawableState();
								if(CurrentLives == 0)
								{
									GC.StopThread();
									Intent gameOverIntent = new Intent(Smack_A_CelebrityActivity.this,GameOver.class);
									startActivity(gameOverIntent);
									finish();
								}
							}

						}

					});

				}

			}
		});
		GC = new GameCore(step);
		GC.start();
	}

	public Activity getActivity() {
		return this.getActivity();
	}

	private static class ChangeImage extends Handler {
		SpotLight spotlight = new SpotLight();
		private int oldLocation = -1;
		@Override
		public void handleMessage(Message message) {
			if(numberof != 0)
			current = randomNumber(0, 8);
			else
				current = 0;
			Bundle bundle = message.getData();
			currentCelebrityLocation = bundle.getInt("newPosition");
			if(Math.random() < probability){
			GCelebrity.setItem(currentCelebrityLocation,CelebritiesArray[current]);
			IsCelebrity = true;
			}
			else
			{
				GCelebrity.setItem(currentCelebrityLocation,favCelebrity);
				IsCelebrity = false;
			}
			if (oldLocation != -1 && currentCelebrityLocation != oldLocation) {
				GCelebrity.setItem(oldLocation, spotlight.getSpotLight());
			}

			oldLocation = currentCelebrityLocation;
			GCelebrity.notifyDataSetChanged();

		}

	}

	public static int randomNumber(int min, int max) {
		return min + (new Random()).nextInt(max - min);
	}
	public void setProbability(double probability){
		Smack_A_CelebrityActivity.probability=probability;
	}

	public void setUp(){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		String numberToSmack = sp.getString("numberToSmack", "1");
		numberof = Integer.parseInt(numberToSmack) -1;
		try{
		String fav = sp.getString("favoriteCelebrity", "stari");
		Class<drawable> resu = R.drawable.class;
		Field favField = resu.getField(fav);
		favCelebrity = favField.getInt(null);
		} catch(Exception e) {
			Log.e("MyTag", "Failed", e);
		}

		for (int num = 0; num < 8 ; num++) {

			String icon = sp.getString("listPref" + (num + 1), "kardashian");

			try {
				Class<drawable> res = R.drawable.class;
				Field field = res.getField(icon);
				CelebritiesArray[num] = field.getInt(null);
			} catch (Exception e) {
				Log.e("MyTag", "Failed", e);
			}

		}
	}

}