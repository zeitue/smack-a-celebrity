package com.ice_os.android.smack_a_celebrity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


public class GraphicsCelebrity extends BaseAdapter implements View.OnClickListener {
	private Context celebrityContext;

	public GraphicsCelebrity(Context c) {
		celebrityContext = c;
	}

	@Override
	public boolean areAllItemsEnabled() {

		return super.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {

		return super.isEnabled(position);
	}

	public int getCount() {
		return CelebrityThumbIDs.length;
	}

	public Integer getItem(int position) {

		Integer item = 0;

		for (int i = 0; i < getCount(); i++) {
			if (CelebrityThumbIDs[i].equals(item)) {
				item = CelebrityThumbIDs[i];
			}
		}

		return item;
	}

	public void setItem(Integer index, Integer item) {
		CelebrityThumbIDs[index] = item;
		//System.err.println(item);
	}

	public long getItemId(int position) {

		long item = 0;

		for (int i = 0; i < getCount(); i++) {
			if (CelebrityThumbIDs[i].equals(position)) {
				item = CelebrityThumbIDs[i];
			}
		}
		return item;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { 
			imageView = new ImageView(celebrityContext);
			imageView.setLayoutParams(new GridView.LayoutParams(128, 128));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(7, 7, 7, 7);
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(CelebrityThumbIDs[position]);

		return imageView;
	}

	
	private Integer[] CelebrityThumbIDs = { R.drawable.spotlight, R.drawable.spotlight,
			R.drawable.spotlight, R.drawable.spotlight, R.drawable.spotlight, R.drawable.spotlight, R.drawable.spotlight, R.drawable.spotlight

	};

	public Integer[] getCelebrityThumbIDs() {

		return CelebrityThumbIDs;
	}

	public void onClick(View v) {
		
		this.notifyDataSetChanged();

	}

}